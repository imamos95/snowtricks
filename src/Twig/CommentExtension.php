<?php

namespace App\Twig;

use App\Entity\Comment;
use App\Entity\User;
use App\Repository\CommentRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CommentExtension extends AbstractExtension
{

    public function __construct(private readonly CommentRepository $commentRepository)
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('user_comments', $this->userComments(...)),
        ];
    }

    /**
     * @return array<Comment>
     */
    public function userComments(User $user): array
    {
        return $this->commentRepository->findBy(['author' => $user]);
    }

}